#!/bin/bash

#SBATCH --nodes 1
#SBATCH --ntasks-per-node 4
##SBATCH --cpus-per-task 8 
#SBATCH --time 00:15:00
#SBATCH -p scitas
##SBATCH -p debug

module purge
#module load intelmpi/5.1.1 intel scorep fftw totalview
module load intel mvapich2/2.0.1/debug-intel-15.0.2.164 totalview


#export I_MPI_ADJUST_ALLTOALL=4
#export I_MPI_ADJUST_ALLREDUCE=4
#export MALLOC_MMAP_MAX_=0
#export MALLOC_TRIM_THRESHOLD_=-1
#export I_MPI_FABRICS=shm:tmi

export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1
export OMP_NESTED=false
export MKL_DYNAMIC=false


#echo pippo
#cd /scratch/scitas/nvarini/test_PW_small_benchmark
#export SCOREP_EXPERIMENT_DIRECTORY=test4-fftw-3.3.4-mpi
#srun -n 4 /scratch/scitas/nvarini/espresso_trunk_fftw3/PW/src/pw.x < test_4.in >  ${SCOREP_EXPERIMENT_DIRECTORY}.out


export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1

cd /scratch/scitas/nvarini/espresso_adios/vlocpsi_test

export LD_LIBRARY_PATH=/scratch/scitas/nvarini/adios-1.8.0/lib:/scratch/scitas/nvarini/mxml-2.9/lib:${LD_LIBRARY_PATH}
rm -rf primo secondo terzo
rm fort*
mkdir primo
mkdir secondo
mkdir terzo
#srun -n 1 /scratch/scitas/nvarini/espresso_adio/PW/src/pw.x < test_4.in > ${SCOREP_EXPERIMENT_DIRECTORY}.out
#tvscript -event_action "error=>display_backtrace -show_arguments -show_locals" -mpi SLURM -tasks ${SLURM_NTASKS}  /scratch/scitas/nvarini/espresso_adios/PW/src/pw.x < test_4.in 2>&1|tee test_4.out
#srun -n 2   /scratch/scitas/nvarini/espresso_adios/PW/src/pw.x < test_4.in 2>&1|tee test_4.out.4
#tvscript -event_action "error=>display_backtrace -show_arguments -show_locals" -mpi SLURM -tasks ${SLURM_NTASKS}  /scratch/scitas/nvarini/espresso_adios/PW/src/pw.x < test_4.in 2>&1|tee test_4.out
#mv fort2* terzo
srun   /scratch/scitas/nvarini/espresso_adios/PW/src/pw.x < test_4.in 2>&1|tee test_4.out.1
mv fort.* primo
tvscript -event_action "error=>display_backtrace -show_arguments -show_locals" -mpi SLURM -tasks ${SLURM_NTASKS} ./vloc_psi.x > vloc_psi.out
#tvscript -script_file tvscript -memory_debugging -event_action "termination_notification=list_leaks" -mpi SLURM -tasks ${SLURM_NTASKS} ./vloc_psi.x > vloc_psi.out
#srun   /scratch/scitas/nvarini/espresso_adios/vlocpsi_test/vloc_psi.x > vloc_psi.out
#srun ./a.out > output
mv fort.* secondo
#srun  ./vloc_psi.x







#command from runme

