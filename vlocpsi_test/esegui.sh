#!/bin/bash 
export PATH=/scratch/scitas/nvarini/adios-1.8.0/bin:$PATH
export LD_LIBRARY_PATH=/scratch/scitas/nvarini/adios-1.8.0/lib:/scratch/scitas/nvarini/mxml-2.9/lib:${LD_LIBRARY_PATH}
rm -rf primo secondo
mkdir primo
mkdir secondo
export OMP_NUM_THREADS=1
srun  /scratch/scitas/nvarini/espresso_adios/PW/src/pw.x < test_4.in 2>&1|tee test_4.out
cp fort.* primo
#srun ./vloc_psi.x
cp fort.* secondo

