#/bin/bash

export ADIOS_INC=/scratch/scitas/nvarini/adios-1.8.0/include
export ADIOS_LIB=/scratch/scitas/nvarini/adios-1.8.0/lib
export ESPRESSO_DIR=/scratch/scitas/nvarini/espresso_adios/

#mpiifort -DHAVE_CONFIG_H -I. -I${ADIOS_INC}      -g -c -o arrays_write.o arrays_write.F90
#mpiifort  -g -L/scratch/scitas/nvarini/mxml-2.9/lib   -o arrays_write arrays_write.o ${ADIOS_LIB}/libadiosf.a -lm -lmxml  -libverbs  
#mpiifort -g -o arrays_write arrays_write.o  -L/scratch/scitas/nvarini/mxml-2.9/lib ${ADIOS_LIB}/libadiosf.a -lm -lmxml -libverbs
#mpiifort -DHAVE_CONFIG_H -I. -I${ADIOS_INC}   -g -c -o arrays_read.o arrays_read.F90
#mpiifort  -g   -o arrays_read arrays_read.o ${ADIOS_LIB}/libadiosreadf.a -lm  -libverbs  
#mpiifort -g -o arrays_read arrays_read.o  ${ADIOS_LIB}/libadiosreadf.a -lm -libverbs


#mpiifort -O0 -g -I${ADIOS_INC} -openmp -assume byterecl -fpp -I/scratch/scitas/nvarini/espresso_adios/Modules -I/scratch/scitas/nvarini/espresso_adios/include -nomodule -g -c -o vloc_psi.o vloc_psi.f90
#mpiifort -O0 -openmp -g -L/scratch/scitas/nvarini/mxml-2.9/lib   -o vloc_psi.x vloc_psi.o ${ADIOS_LIB}/libadiosf.a -lm -lmxml  -libverbs /scratch/scitas/nvarini/espresso_adios/Modules/libqemod.a  /scratch/scitas/nvarini/espresso_adios/PW/src/libpw.a  /scratch/scitas/nvarini/espresso/clib/clib.a  /scratch/scitas/nvarini/espresso/flib/flib.a /scratch/scitas/nvarini/espresso/flib/ptools.a -L/ssoft/intel/15.0.0/RH6/all/x86_E5v2/composer_xe_2015.2.164/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64   -lmkl_intel_lp64  -lmkl_intel_thread -lmkl_core   /scratch/scitas/nvarini/fftw-3.3.4/lib/libfftw3.a
mpif90 -O0  -g -I${ADIOS_INC} -openmp -assume byterecl -fpp -I. -Iinclude -nomodule -g -c -o vloc_psi.o vloc_psi.f90
mpif90 -O0  -openmp -g -L/scratch/scitas/nvarini/mxml-2.9/lib   -o vloc_psi.x vloc_psi.o ${ADIOS_LIB}/libadiosf.a -lm -lmxml  -libverbs ${ESPRESSO_DIR}lib/libqemod.a  ${ESPRESSO_DIR}lib/libpw.a  ${ESPRESSO_DIR}lib/clib.a  ${ESPRESSO_DIR}lib/flib.a ${ESPRESSO_DIR}lib/ptools.a -L/ssoft/intel/15.0.0/RH6/all/x86_E5v2/composer_xe_2015.2.164/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64   -lmkl_intel_lp64  -lmkl_intel_thread -lmkl_core   /scratch/scitas/nvarini/fftw-3.3.4/lib/libfftw3.a 
