MODULE vloc_data
    USE wavefunctions_module,  ONLY: psic
    integer, dimension(:), allocatable          :: nls
    integer, dimension(:), allocatable          :: igk 
    !integer :: ngms, npwx
END MODULE vloc_data



program vloc_psi_bench
    use adios_read_mod
    USE mp
    USE kinds
    USE stick_set,  ONLY : pstickset
    USE fft_base,   ONLY : dfftp, dffts
    USE mp_global,  ONLY : mp_startup
    USE mp_bands,   ONLY : me_bgrp, root_bgrp, nproc_bgrp, intra_bgrp_comm
    USE gvect,      ONLY : gvect_init, g, ngm, ngm_g, ngmx, gcutm
    USE gvecs,      ONLY : gvecs_init, ngms, ngms_g, ngsx, gcutms
    USE parameters, ONLY : npk
    USE klist,      ONLY : xk, nks, ngk
    USE wvfct,      ONLY : ecutwfc, npwx, npw
    USE cell_base,  ONLY : tpiba2, bg, at
    USE recvec_subs,        ONLY : ggen
    USE vloc_data
    USE grid_subroutines,   ONLY : realspace_grid_init



    implicit none
    save
    include 'mpif.h'
    character(len=25)   :: filename = "vlocpsi.bp"
    character(len=25)   :: filename_fft = "fftdesc.bp"
    integer             :: rank, size, i, j, ierr, nproc
    integer             :: comm, ngw_, ngm_, ngs_
    double precision    :: gkcut
    logical             :: gamma_only
    integer             :: nimage_, npool_, npot_, ndiag_, nband_, ntg_
    integer             :: ntask_groups
    ! ADIOS variables declarations for matching gwrite_temperature.fh 
    integer*8               :: f, t, f_lda
    integer                 :: method = ADIOS_READ_METHOD_BP
    integer*8               :: sel, sel_lda
    integer*8, dimension(2) :: readsize=1, offset=0

    ! variables to read in 
    integer                 :: lda, m, n, errore !local indexes
    integer                 :: lda_global, npwx_global, ngms_global,nnrs 
    integer                 :: bgrp_comm
    integer                 :: dffts_global, dfftp_global, offx_lda,nnrp
    integer                 :: offx_ngms, offx_npwx, offx_dffts, offx_dfftp
    integer                 :: lda_g, m_g !gather indexes
    integer                 :: offx_lda_local, offx_dfftp_local, offx_dffts_local
    integer                 :: offx_ngms_local
    double complex, dimension(:,:), allocatable :: psi
    double complex, dimension(:,:), allocatable :: hpsi
    real(dp), dimension(:), allocatable         :: v

    call MPI_Init (ierr)
    call MPI_Comm_dup (MPI_COMM_WORLD, comm, ierr)
    call MPI_Comm_rank (comm, rank, ierr)
    call MPI_Comm_size (comm, nproc, ierr)
    call adios_read_init_method (method, comm, "verbose=3", ierr);
  

    !write (*,'("rank=",i0," NX=",i0," NY=",i0)') rank, lda, m
    ! select specific writer's data (using rank since each rank of the writer
    ! wrote one block of the data)

    call adios_read_open (t, filename_fft, method, comm, ADIOS_LOCKMODE_NONE, 1.0, ierr);
    call adios_selection_writeblock (sel, 0)
    call adios_schedule_read (t, sel, "gamma_only", 0, 1, gamma_only, ierr)
    call adios_schedule_read (t, sel, "bg", 0, 1, bg, ierr)
    call adios_schedule_read (t, sel, "at", 0, 1, at, ierr)
    call adios_schedule_read (t, sel, "gcutm", 0, 1, gcutm, ierr)
    call adios_schedule_read (t, sel, "gkcut", 0, 1, gkcut, ierr)
    call adios_schedule_read (t, sel, "gcutms", 0, 1, gcutms, ierr)
    call adios_schedule_read (t, sel, "ngw_", 0, 1, ngw_, ierr)
    call adios_schedule_read (t, sel, "ngm_", 0, 1, ngm_, ierr)
    call adios_schedule_read (t, sel, "ngs_", 0, 1, ngs_, ierr)
    !call adios_schedule_read (t, sel, "nimage_", 0, 1, nimage_, ierr)
    call adios_get_scalar (t,  "nimage_", nimage_, ierr)
    call adios_schedule_read (t, sel, "npool_", 0, 1, npool_, ierr)
    call adios_schedule_read (t, sel, "npot_", 0, 1, npot_, ierr)
    call adios_schedule_read (t, sel, "ndiag_", 0, 1, ndiag_, ierr)
    call adios_schedule_read (t, sel, "nband_", 0, 1, nband_, ierr)
    call adios_schedule_read (t, sel, "ntg_", 0, 1, ntg_, ierr)
    !call adios_schedule_read (t, sel, "dfftp%nr1", 0, 1, dfftp%nr1, ierr)
    !call adios_schedule_read (t, sel, "dfftp%nr1x", 0, 1, dfftp%nr1x, ierr)
    !call adios_schedule_read (t, sel, "dfftp%nr2", 0, 1, dfftp%nr2, ierr)
    !call adios_schedule_read (t, sel, "dfftp%nr2x", 0, 1, dfftp%nr2x, ierr)
    !call adios_schedule_read (t, sel, "dfftp%nr3", 0, 1, dfftp%nr3, ierr)
    !call adios_schedule_read (t, sel, "dfftp%nr3x", 0, 1, dfftp%nr3x, ierr)
    !call adios_schedule_read (t, sel, "dfftp%nnr", 0, 1, dfftp%nnr, ierr)
    !call adios_schedule_read (t, sel, "dffts%nr1", 0, 1, dffts%nr1, ierr)
    !call adios_schedule_read (t, sel, "dffts%nr1x", 0, 1, dffts%nr1x, ierr)
    !call adios_schedule_read (t, sel, "dffts%nr2", 0, 1, dffts%nr2, ierr)
    !call adios_schedule_read (t, sel, "dffts%nr2x", 0, 1, dffts%nr2x, ierr)
    !call adios_schedule_read (t, sel, "dffts%nr3", 0, 1, dffts%nr3, ierr)
    !call adios_schedule_read (t, sel, "dffts%nr3x", 0, 1, dffts%nr3x, ierr)
    !call adios_schedule_read (t, sel, "dffts%nnr", 0, 1, dffts%nnr, ierr)
    call adios_schedule_read (t, sel, "ntask_groups", 0, 1, ntask_groups, ierr)
    !call adios_schedule_read (t, sel, "intra_bgrp_comm", 0, 1, bgrp_comm, ierr)
    call adios_schedule_read (t, sel, "ecutwfc", 0, 1, ecutwfc, ierr)
    call adios_schedule_read (t, sel, "tpiba2", 0, 1, tpiba2, ierr)
    call adios_schedule_read (t, sel, "xk", 0, 1, xk, ierr)
    call adios_perform_reads (t, ierr)
    call adios_perform_reads (t, ierr)
    call adios_read_close (t, ierr)
    call MPI_Barrier (comm, ierr);
    call mp_startup()
!    CALL allocate_fft()


   

    call adios_read_open (f, filename, method, comm, ADIOS_LOCKMODE_NONE, 1.0, ierr);
    !call adios_selection_writeblock ( rank)

    ! First get the scalars to calculate the size of the arrays.
    ! Note that we cannot use adios_get_scalar here because that
    !   retrieves the same NX for everyone (from writer rank 0).
    call adios_selection_delete (sel) 
    call adios_selection_writeblock (sel, 0)
    !call adios_schedule_read (f, sel, "lda", 0, 1, lda, ierr)
    !call adios_get_scalar (f, "lda" ,lda, ierr)
    call adios_schedule_read (f, sel, "m", 0, 1, m, ierr)
    !call adios_schedule_read (f, sel, "n", 0, 1, n, ierr)
    !call adios_schedule_read (f, sel, "ngms", 0, 1, ngms, ierr)
    call adios_schedule_read (f, sel, "nks", 0, 1, nks, ierr)
    !call adios_schedule_read (f, sel, "npwx", 0, 1, npwx, ierr)
    !call adios_schedule_read (f, sel, "dfftp%nnr", 0, 1, nnrp, ierr)
    !call adios_schedule_read (f, sel, "dffts%nnr", 0, 1, nnrs, ierr)
    !call adios_schedule_read (f, sel, "offx_lda", 0, 1, offx_lda, ierr)
    !call adios_schedule_read (f, sel, "offx_npwx", 0, 1, offx_npwx, ierr)
    !call adios_schedule_read (f, sel, "offx_ngms", 0, 1, offx_ngms, ierr)
    !call adios_schedule_read (f, sel, "offx_dfftp", 0, 1, offx_dfftp, ierr)
    !call adios_schedule_read (f, sel, "offx_dffts", 0, 1, offx_dffts, ierr)
    call adios_schedule_read (f, sel, "dffts_global", 0, 1, dffts_global, ierr)
    call adios_schedule_read (f, sel, "dfftp_global", 0, 1, dfftp_global, ierr)
    call adios_schedule_read (f, sel, "lda_global", 0, 1, lda_global, ierr)
    call adios_schedule_read (f, sel, "ngms_global", 0, 1, ngms_global, ierr)
    call adios_schedule_read (f, sel, "npwx_global", 0, 1, npwx_global, ierr)
    call adios_perform_reads (f, ierr)
    allocate(ngk(nks))
    call adios_schedule_read (f, sel, "ngk", 0, 1, ngk, ierr)
    call adios_perform_reads (f, ierr)
    !dfftp%nnr=nnrp
    !dffts%nnr=nnrs
    call adios_selection_delete (sel) 
    
    CALL realspace_grid_init ( dfftp, at, bg, gcutm )
    IF ( gcutms == gcutm ) THEN
     ! ... No double grid, the two grids are the same
      dffts%nr1 = dfftp%nr1 ; dffts%nr2 = dfftp%nr2 ; dffts%nr3 = dfftp%nr3
      dffts%nr1x= dfftp%nr1x; dffts%nr2x= dfftp%nr2x; dffts%nr3x= dfftp%nr3x
     ELSE
      CALL realspace_grid_init ( dffts, at, bg, gcutms)
    END IF

    CALL pstickset( gamma_only, bg, gcutm, gkcut, gcutms, &
                  dfftp, dffts, ngw_ , ngm_ , ngs_ , me_bgrp, &
                  root_bgrp, nproc_bgrp, intra_bgrp_comm, ntask_groups )

    call gvect_init(ngm_ , intra_bgrp_comm)
    call gvecs_init(ngs_ , intra_bgrp_comm );
    CALL ggen( gamma_only, at, bg )
    write(rank+200,*) dfftp%nnr
!
    call n_plane_waves (ecutwfc, tpiba2, nks, xk, g, ngm_, npwx, ngk)
    npw=ngk(1)

    call prepare_index_adios(npw,offx_lda_local,npwx-1,comm,nproc)
    call prepare_index_adios(dfftp%nnr,offx_dfftp_local,dfftp_global,comm,nproc)
    call prepare_index_adios(dffts%nnr,offx_dffts_local,dffts_global,comm,nproc)
    call prepare_index_adios(ngms,offx_ngms_local,ngms_g,comm,nproc)
                    
    ! Allocate space for the arrays
    !allocate (psi(lda,m))
    !it must be change to be independent from the number of processors
    !readsize(1)=lda
    !readsize(2)=m
    readsize(1)=npwx
    readsize(2)=m
    !offset(1)=offx_lda
    offset(1)=offx_lda_local
    !offset(1)=npw
    offset(2)=0
    allocate (psi(readsize(1),readsize(2)))
    allocate (hpsi(readsize(1),readsize(2)))
    allocate (psic(dfftp%nnr))
    ALLOCATE( nls (ngms) )
    ALLOCATE( igk (npwx) )
    allocate(v(dffts%nnr))


    write(rank+300,*) readsize, offset
    ! Read the arrays
    call adios_selection_boundingbox (sel_lda, 2, offset, readsize)
    call adios_schedule_read (f, sel_lda, "psi", 0, 1, psi, ierr)
    call adios_schedule_read (f, sel_lda, "hpsi", 0, 1, hpsi, ierr)
    call adios_perform_reads (f, ierr)
    call adios_selection_delete (sel_lda) 
    !write(rank+400,*) psi

    readsize(1)=dfftp%nnr
    !offset(1)=offx_dfftp
    offset(1)=offx_dfftp_local
    call adios_selection_boundingbox (sel_lda, 1, offset(1),readsize(1))
    call adios_schedule_read (f, sel_lda, "psic", 0, 1, psic, ierr)
    call adios_perform_reads (f, ierr)
    call adios_selection_delete (sel_lda) 

    readsize(1)=ngms
    !offset(1)=offx_ngms
    offset(1)=offx_ngms_local
    call adios_selection_boundingbox (sel_lda, 1, offset(1),readsize(1))
    call adios_schedule_read (f, sel_lda, "nls", 0, 1, nls, ierr)
    call adios_perform_reads (f, ierr)
    call adios_selection_delete (sel_lda) 

    readsize(1)=npwx
    !offset(1)=offx_npwx
    offset(1)=offx_lda_local
    call adios_selection_boundingbox (sel_lda, 1, offset(1),readsize(1))
    call adios_schedule_read (f, sel_lda, "igk", 0, 1, igk, ierr)
    call adios_perform_reads (f, ierr)
    call adios_selection_delete (sel_lda) 

    readsize(1)=dffts%nnr
    !offset(1)=offx_dffts
    offset(1)=offx_dffts_local
    call adios_selection_boundingbox (sel_lda, 1, offset(1),readsize(1))
    call adios_schedule_read (f, sel_lda, "v", 0, 1, v, ierr)
    call adios_perform_reads (f, ierr)
    call adios_selection_delete (sel_lda) 
    call adios_read_close (f, ierr)
    call adios_read_finalize_method (method, ierr)
    !CALL vloc_psi(lda,n,m,psi,hpsi,v)
    CALL vloc_psi(npwx,npwx,m,psi,hpsi,v,npwx_global)

 

    deallocate(psi)
    deallocate(hpsi)
    deallocate(psic)
    deallocate(igk)
    deallocate(nls)
    deallocate(v)
    deallocate(ngk)
    call MPI_Finalize (ierr);



end program  vloc_psi_bench


subroutine vloc_psi(lda, n, m, psi, hpsi, v, npwx_global)

USE kinds,         ONLY : DP
USE fft_base,      ONLY : dffts,dfftp, tg_gather
USE fft_interfaces,ONLY : fwfft, invfft
USE vloc_data
USE mp_world,     ONLY : mpime, nproc
USE wavefunctions_module, ONLY : psic
USE parallel_include
USE wvfct,        ONLY : nbnd, npwx
USE mp_bands,     ONLY : intra_bgrp_comm
USE mp_global,    ONLY : nproc_bgrp 
USE mp_wave
USE mp,           ONLY : mp_sum

implicit none
integer, intent(in) :: lda,n,m, npwx_global
complex(DP), intent(in) :: psi(lda,m)
complex(DP), intent(inout) :: hpsi(lda,m)
complex(DP), allocatable :: psi_r(:,:), psitmp(:), psitot(:,:), hpsitot(:,:)
REAL(DP), INTENT(in) :: v(dffts%nnr)
integer :: ibnd, j, counter, rank, ierr, offset, proc, npwx_tot, ij, i 
integer :: ngpwpp(nproc_bgrp), total, nstat
integer, allocatable :: sendcnt(:), recvcnt(:), sdisp(:), rdisp(:), ns(:), igk_tmp(:)
 


  counter=0
  j=0
  rank=mpime
  !write(rank+100,*) psi
  !write(rank+200,*) hpsi
  !write(rank+300,*) n,m
  !write(rank+400,*) v
  !write(rank+500,*) psic
  allocate(sendcnt(nproc))
  allocate(recvcnt(nproc))
  allocate(sdisp(nproc))
  allocate(rdisp(nproc))
  allocate(psi_r(m,npwx))
  allocate(psitmp(m*npwx))
  ALLOCATE( ns( nproc_bgrp ) )

  ns=m/nproc_bgrp

  DO j = 1, m
     IF( (j-1) < MOD( m, nproc_bgrp ) ) ns( j ) = ns( j ) + 1 
  END DO
  !ns = 61
  nstat = ns( mpime +1)
  !nstat = 61

  !do i=1, npwx
  !  do j=1, m
  !     psi_r(j,i) = psi(i,j)
  !  enddo
  !enddo

  total = 0
  DO proc=1,nproc_bgrp
     ngpwpp(proc)=dfftp%nwl(proc)
     total=total+ngpwpp(proc)
  END DO
  write(mpime+400,*) total, nstat

  ALLOCATE(psitot(total,nstat))
  ALLOCATE(hpsitot(total,nstat))
  ALLOCATE(igk_tmp(total))

  !call mpi_scan(npwx*m,offset,1,MPI_INTEGER,MPI_SUM,intra_bgrp_comm,ierr)
  !offset = offset-npwx*m
  !call mpi_allgather(offset,1,MPI_INTEGER,sdisp,1,MPI_INTEGER,intra_bgrp_comm,ierr)
  !rdisp = sdisp
  !call mpi_allgather(npwx*m,1,MPI_INTEGER,sendcnt,1,MPI_INTEGER,intra_bgrp_comm,ierr)
  !scnt(:) = 1000
  !write(rank+700,*) sendcnt
  !write(rank+710,*) recvcnt
  !call errore('','',1)
 
  call mpi_allgather(npwx,1,MPI_INTEGER,recvcnt,1,MPI_INTEGER,intra_bgrp_comm,ierr)
   !DO proc=1,nproc
   !   sendcnt(proc) = npwx
   !   recvcnt(proc) = npwx
   !END DO
   sdisp(1)=0
   rdisp(1)=0
   DO proc=2,nproc
      !sdisp(proc) = sdisp(proc-1) + sendcnt(proc-1)
      rdisp(proc) = rdisp(proc-1) + recvcnt(proc-1)
   END DO
   call MPI_Allgatherv(igk,npwx,MPI_INTEGER,igk_tmp,recvcnt,rdisp,MPI_INTEGER,intra_bgrp_comm,ierr)
  
  !do j=0, nproc-1
  !  sendcnt(j) = npwx*m
  !  recvcnt(j) = npwx*m
  !  sdisp(j) = offset
  !  rdisp(j) = offset
  !enddo
  
 

  !write(rank+900,*) sdisp
  !write(rank+910,*) rdisp
  !call mpi_alltoallv(psi,sendcnt,sdisp,MPI_DOUBLE_COMPLEX, &
  !                  psi_r,recvcnt,rdisp,MPI_DOUBLE_COMPLEX,intra_bgrp_comm,ierr) 
  !CALL MPI_ALLTOALLV( psi, sendcnt, sdisp, MPI_DOUBLE_COMPLEX,             &
  !         &             psi_r, recvcnt, rdisp, MPI_DOUBLE_COMPLEX, intra_bgrp_comm, ierr)
  
  !write(rank+600,*) psi_r
  !write(rank+700,*) psi

  !write(mpime+1000,*) size(psi(1,:)), size(psi(:,1)), & 
  !               size(psitot(1,:)), size(psitot(:,1)), &
  !               size(igk(:)), size(nls(:))
                 
  write(mpime+1000,*) ngpwpp
  write(mpime+2000,*)  ns

  CALL redistwf( psi, psitot, ngpwpp, ns, intra_bgrp_comm, 1 )
  CALL redistwf( hpsi, hpsitot, ngpwpp, ns, intra_bgrp_comm, 1 )
  write(mpime+1000,*) sum(psi)
  write(mpime+2000,*) sum(psitot)
  write(mpime+3000,*) sum(igk), sum(igk_tmp)

do ibnd=1, m
!do ibnd=1, nstat

  psic = (0.0d0,0.0d0)
  psic(nls(igk(1:n))) = psi(1:n,ibnd)
  !psic(nls(igk(1:n))) = psi_r(ibnd,1:n)
  !psic(nls(igk_tmp(1:total))) = psitot(1:total,ibnd)
        !write(rank+400,*) sum(psic), sum(v), dffts%nnr
  

  call invfft('Wave',psic,dffts)
  write(mpime+100,*) ibnd,counter, nstat, sum(psic), sum(nls)
  psic(nls(igk(1:n))) = psitot(1:n,ibnd)
  call invfft('Wave',psic,dffts)
  write(mpime+100,*) ibnd,counter, nstat, sum(psic), sum(nls)
 
  call errore('','',1)

  do j=1, dffts%nnr
    psic(j) = psic(j)*v(j)
    !write(mpime+200,*) ibnd,psic(j)
  enddo
  counter=counter+1


  CALL fwfft ('Wave', psic, dffts)

  !do j=1, n
  do j=1, total
    !hpsi(j,ibnd) = hpsi(j,ibnd) + psic(nls(igk(j)))
    hpsitot(j,ibnd) = hpsitot(j,ibnd) + psic(nls(igk_tmp(j)))
  enddo

enddo
write(mpime+100,*) sum(hpsitot), sum(hpsi)


deallocate(sendcnt,recvcnt,sdisp,rdisp,psi_r)
deallocate(ns,psitot,igk_tmp,hpsitot)


end subroutine vloc_psi


SUBROUTINE prepare_index_adios(sendm,recm,globalm,comm,nproc)

  USE parallel_include
  USE mp, ONLY : mp_sum
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: comm, nproc
  INTEGER, INTENT(INOUT) :: sendm, recm, globalm
  INTEGER :: errore

  call mpi_scan(sendm,recm,1,MPI_INTEGER,MPI_SUM,comm,errore)
  recm=recm-sendm
  globalm=sendm
  call mp_sum(globalm,comm)

END SUBROUTINE prepare_index_adios


